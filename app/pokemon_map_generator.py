import geopandas as gpd
import matplotlib.pyplot as plt
import requests
from io import BytesIO
from shapely.geometry import box, Point, LineString, Polygon
import json

def get_landmarks_from_kml(url):
    response = requests.get(url)
    kml_data = gpd.read_file(BytesIO(response.content), driver='KML')
    return kml_data

def get_osm_data(query):
    overpass_url = "https://overpass-api.de/api/interpreter"
    response = requests.get(overpass_url, params={'data': query})
    data = response.json()
    return data

def create_gdf_from_osm(data, geometry_type):
    features = []
    for element in data['elements']:
        if element['type'] == 'way':
            coords = [(node['lon'], node['lat']) for node in element['geometry']]
            if geometry_type == 'Polygon':
                if coords[0] == coords[-1]:  # ポリゴンは閉じている必要がある
                    geom = Polygon(coords)
                else:
                    geom = LineString(coords)  # 閉じていない場合は LineString として扱う
            else:
                geom = LineString(coords)
            features.append({'geometry': geom})
    return gpd.GeoDataFrame(features, crs="EPSG:4326")

def generate_pokemon_style_map(center_point, dist=1000, landmarks_url=None):
    print(f"地図データを取得中... (中心点: {center_point}, 半径: {dist}m)")
    
    lat, lon = center_point
    delta = dist / 111000  # 緯度経度の概算変換（1度あたり約111km）

    # OpenStreetMapからデータを取得
    building_query = f"[out:json];way[building]({lat-delta},{lon-delta},{lat+delta},{lon+delta});(._;>;);out geom;"
    road_query = f"[out:json];way[highway]({lat-delta},{lon-delta},{lat+delta},{lon+delta});(._;>;);out geom;"

    building_data = get_osm_data(building_query)
    road_data = get_osm_data(road_query)

    buildings = create_gdf_from_osm(building_data, 'Polygon')
    roads = create_gdf_from_osm(road_data, 'LineString')

    # バウンディングボックスを作成
    bbox = box(lon-delta, lat-delta, lon+delta, lat+delta)
    area = gpd.GeoDataFrame({'geometry': [bbox]}, crs="EPSG:4326")

    # WebメルカトルにCRS変換
    area = area.to_crs(epsg=3857)
    buildings = buildings.to_crs(epsg=3857)
    roads = roads.to_crs(epsg=3857)

    print("地図を描画中...")
    fig, ax = plt.subplots(figsize=(15, 15))

    # 背景を描画
    area.plot(ax=ax, facecolor='#5DBE3F', edgecolor='none')

    # 建物を描画
    buildings.plot(ax=ax, facecolor='#FFD700', edgecolor='#8B4513', linewidth=1, alpha=0.7)

    # 道路を描画
    roads.plot(ax=ax, color='#FFD700', linewidth=2)

    # ランドマークを描画
    if landmarks_url:
        landmarks = get_landmarks_from_kml(landmarks_url)
        landmarks = landmarks.to_crs(epsg=3857)
        landmarks.plot(ax=ax, color='red', markersize=100, alpha=0.7)

    # 水域の表現（簡略化のため、地図の端に青い円を追加）
    xmin, ymin, xmax, ymax = area.total_bounds
    circle1 = plt.Circle((xmin, ymin), (xmax-xmin)/10, fc='#43B0F1', ec='none', zorder=5)
    circle2 = plt.Circle((xmax, ymax), (xmax-xmin)/10, fc='#43B0F1', ec='none', zorder=5)
    ax.add_patch(circle1)
    ax.add_patch(circle2)

    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    ax.axis('off')
    plt.title("ポケモンタウンマップ", fontsize=20, fontweight='bold', fontname='MS Gothic')

    print("画像を保存中...")
    plt.tight_layout()
    filename = 'pokemon_style_town_map_with_landmarks.png'
    plt.savefig(filename, dpi=300, bbox_inches='tight', facecolor='#43B0F1')
    plt.close()

    print(f"ポケモンスタイルのタウンマップ（ランドマーク付き）が生成されました。")
    print(f"ファイル名: {filename}")

if __name__ == "__main__":
    ebitsuka_center = (34.6974511, 137.7302235)
    landmarks_url = "https://www.google.com/maps/d/u/0/kml?mid=13p6K53qaVydh94FXEfLr4GLMIGChJ-U&forcekml=1"
    
    generate_pokemon_style_map(ebitsuka_center, dist=1000, landmarks_url=landmarks_url)